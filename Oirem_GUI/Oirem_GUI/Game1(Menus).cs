﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1_Temp: Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont font;
        Texture2D button;
        Texture2D overlay;
        Point screenCenter;
        Rectangle windowSize;
        MainMenu menu;
        GameScreen game;
        GameOver gameOver;
        PauseMenu pause;
        OptionsMenu options;
        MouseState mouse;
        MouseState oldMouse;
        KeyboardState kbState;
        KeyboardState oldKbState;

        enum Choices
        {
            MainMenu,
            Options,
            PlayGame,
            PauseMenu,
            GameOver,
            Quit
        }

        Choices choice;

        public Game1_Temp()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            IsMouseVisible = true;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            font = Content.Load<SpriteFont>("mainFont");
            button = Content.Load<Texture2D>("Parallelogon_rectangle");
            overlay = Content.Load<Texture2D>("Colorful_rectangle");
            screenCenter = new Point(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2);
            windowSize = GraphicsDevice.Viewport.Bounds;
            menu = new MainMenu(button, overlay, screenCenter, windowSize);
            game = new GameScreen(button, overlay, screenCenter, windowSize);
            pause = new PauseMenu(button, overlay, screenCenter, windowSize);
            options = new OptionsMenu(button, overlay, screenCenter, windowSize);
            gameOver = new GameOver(button, overlay, screenCenter, windowSize);
            choice = Choices.MainMenu;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            mouse = Mouse.GetState();
            kbState = Keyboard.GetState();

            switch (choice)
            {
                case Choices.Quit:
                    Exit();
                    break;
                case Choices.MainMenu:
                    if (menu.Play.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.PlayGame;
                    }
                    if (menu.Options.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.Options;
                    }
                    if (menu.Quit.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.Quit;
                    }
                    break;
                case Choices.Options:
                    if (options.Main.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.MainMenu;
                    }
                    break;
                case Choices.PlayGame:
                    if (game.Pause.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released ||
                        kbState.IsKeyDown(Keys.P) && oldKbState.IsKeyUp(Keys.P))
                    {
                        choice = Choices.PauseMenu;
                    }
                    // For Testing
                    if (kbState.IsKeyDown(Keys.G) && oldKbState.IsKeyUp(Keys.G))
                    {
                        choice = Choices.GameOver;
                    }
                    break;
                case Choices.PauseMenu:
                    if (pause.Resume.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.PlayGame;
                    }
                    if (pause.Options.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.Options;
                    }
                    if (pause.Main.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.MainMenu;
                    }
                    if (pause.Quit.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.Quit;
                    }
                    break;
                case Choices.GameOver:
                    if (gameOver.Retry.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.PlayGame;
                    }
                    if (gameOver.MainMenu.Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released)
                    {
                        choice = Choices.MainMenu;
                    }
                    break;
            }
            base.Update(gameTime);

            oldMouse = mouse;
            oldKbState = kbState;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            switch (choice)
            {
                case Choices.MainMenu:
                    menu.Draw(spriteBatch, font);
                    break;
                case Choices.Options:
                    options.Draw(spriteBatch, font);
                    break;
                case Choices.PlayGame:
                    game.Draw(spriteBatch, font);
                    break;
                case Choices.PauseMenu:
                    pause.Draw(spriteBatch, font);
                    break;
                case Choices.GameOver:
                    gameOver.Draw(spriteBatch, font);
                    break;
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}

