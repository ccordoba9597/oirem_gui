﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem
{
    class GameOver : MenuManager
    {
        Rectangle retry;
        Rectangle mainMenu;

        public Rectangle Retry { get { return retry; } set { retry = value; } }
        public Rectangle MainMenu { get { return mainMenu; } set { mainMenu = value; } }

        public GameOver(Texture2D button, Texture2D overlay, Point screenCtr, Rectangle window) : base(button, overlay, screenCtr, window)
        {

        }

        public override void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            spriteBatch.DrawString(font, "Game Over", CenterText(font, "Game Over", window) - new Vector2(0, 150), Color.Black);
            spriteBatch.Draw(button, retry = new Rectangle(CenterOnScreen(rectangle) + new Point(0, 50), size), Color.White);
            spriteBatch.DrawString(font, "Retry", CenterText(font, "Retry", retry), Color.Black);
            spriteBatch.Draw(button, mainMenu = new Rectangle(CenterOnScreen(rectangle) + new Point(0, 100), size), Color.White);
            spriteBatch.DrawString(font, "Main Menu", CenterText(font, "Main Menu", mainMenu), Color.Black);
        }
    }
}
