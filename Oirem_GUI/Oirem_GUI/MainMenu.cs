﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Threading.Tasks;

namespace Oirem
{
    class MainMenu : MenuManager
    {
        private Rectangle play;
        private Rectangle options;
        private Rectangle quit;
        //private Buttons buttons;

        public Rectangle Play { get { return play; } set { play = value; } }
        public Rectangle Options { get { return options; } }
        public Rectangle Quit { get { return quit; } }

        public MainMenu(Texture2D button, Texture2D overlay, Point screenCtr, Rectangle window) : base(button, overlay, screenCtr, window)
        {
        }

        public override void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            spriteBatch.DrawString(font, "Oirem", CenterText(font, "Oirem", window) - new Vector2(0, 150), Color.Black);
            //buttons.CenterText(font, "Oirem", window, new Vector2(0, -150), spriteBatch, Color.Black);
            spriteBatch.Draw(button, play = new Rectangle(CenterOnScreen(rectangle) + new Point(0, 50), size), Color.White);
            spriteBatch.DrawString(font, "Play", CenterText(font, "Play", play), Color.Black);
            spriteBatch.Draw(button, options = new Rectangle(CenterOnScreen(rectangle) + new Point(0, 100), size), Color.White);
            spriteBatch.DrawString(font, "Options", CenterText(font, "Options", options), Color.Black);
            spriteBatch.Draw(button, quit = new Rectangle(CenterOnScreen(rectangle) + new Point(0, 150), size), Color.White);
            spriteBatch.DrawString(font, "Quit", CenterText(font, "Quit", quit), Color.Black);
        }
    }
}
