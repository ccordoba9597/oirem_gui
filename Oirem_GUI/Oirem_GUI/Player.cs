﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem
{
    class Player
    {
        Texture2D spriteSheet;
        Rectangle original;
        Rectangle orig;
        List<Rectangle> idle;
        List<Rectangle> walking;
        List<Rectangle> jumping;
        List<Rectangle> melee;
        Vector2 velocity;
        Vector2 landing;
        bool isJumping;
        int currentFrame;
        int totalFrames;
        int framesElapsed;
        double timePerFrame = 150;
        const int WIDTH = 60;
        const int HEIGHT = 72;

        enum Anim
        {
            Idle,
            WalkLeft,
            WalkRight,
            Jumping,
            Melee
        }

        enum Direction
        {
            Left,
            Right
        }

        Anim animation;
        Direction direction;

        public Player(Texture2D sprites)
        {
            spriteSheet = sprites;
            original = new Rectangle(0, 200, WIDTH, HEIGHT);
            orig = original;
            idle = new List<Rectangle>();
            walking = new List<Rectangle>();
            jumping = new List<Rectangle>();
            melee = new List<Rectangle>();
            velocity = Vector2.Zero;
            landing = original.Location.ToVector2();
            isJumping = false;
            totalFrames = 1;
            for (int i = 0; i < 3; i++)
            {
                idle.Add(new Rectangle(61 + (76 * i), 15, WIDTH, HEIGHT));
                jumping.Add(new Rectangle(37 + (70 * i), 193, WIDTH, HEIGHT));
                if (i != 0)
                    jumping[i] = new Rectangle(37 + (WIDTH * i), 188, WIDTH + 5, HEIGHT + 7);
            }
            for (int i = 0; i < 4; i++)
            {
                walking.Add(new Rectangle(38 + (80 * i), 104, WIDTH, HEIGHT));
                melee.Add(new Rectangle(240 + (60 * i), 195, WIDTH, HEIGHT));
                if (i > 1)
                    melee[i] = new Rectangle(melee[i - 1].X + melee[i - 1].Width, 195, WIDTH + 22, HEIGHT);
            }
            animation = Anim.Idle;
            direction = Direction.Right;
        }

        public void Update(KeyboardState kbState, KeyboardState oldKbState, GameTime gameTime, Rectangle ground)
        {
            if (kbState.IsKeyDown(Keys.A))
            {
                currentFrame = 1;
                direction = Direction.Left;
                totalFrames = walking.Count;
                animation = Anim.WalkLeft;
                original.X -= 3;
            }
            else if (kbState.IsKeyDown(Keys.D))
            {
                currentFrame = 1;
                direction = Direction.Right;
                totalFrames = walking.Count;
                animation = Anim.WalkRight;
                original.X += 3;
            }
            else
            {
                currentFrame = 1;
                totalFrames = idle.Count;
                animation = Anim.Idle;
            }
            if (kbState.IsKeyDown(Keys.J))
            {
                currentFrame = 1;
                totalFrames = melee.Count;
                animation = Anim.Melee;
            }

            framesElapsed = (int)(gameTime.TotalGameTime.TotalMilliseconds / timePerFrame);
            currentFrame = framesElapsed % totalFrames + 1;

            original.Location += velocity.ToPoint();

            if (!isJumping)
            {
                velocity.Y = 0;
                if (kbState.IsKeyDown(Keys.W) && oldKbState.IsKeyUp(Keys.W))
                {
                    currentFrame = 1;
                    totalFrames = jumping.Count;
                    animation = Anim.Jumping;
                    original.Y -= 30;
                    velocity.Y = -10;
                    isJumping = true;
                }
            }
            else
            {
                int i = 2;
                velocity.Y += .4f * i;
                totalFrames = jumping.Count;
                animation = Anim.Jumping;
                currentFrame = 3;
                if (OnGround(ground))
                {
                    isJumping = false;
                }
            }

            if (currentFrame > totalFrames)
            {
                currentFrame = 1;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            currentFrame = currentFrame - 1;
            switch (direction)
            {
                case Direction.Right:
                    switch (animation)
                    {
                        case Anim.Idle:
                            spriteBatch.Draw(spriteSheet, original, idle[currentFrame], Color.White);
                            break;
                        case Anim.Jumping:
                            spriteBatch.Draw(spriteSheet, original = new Rectangle(original.X, original.Y, original.Width, jumping[currentFrame].Height), jumping[currentFrame], Color.White);
                            break;
                        case Anim.WalkLeft:
                            spriteBatch.Draw(spriteSheet, original, walking[currentFrame], Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
                            break;
                        case Anim.WalkRight:
                            spriteBatch.Draw(spriteSheet, original, walking[currentFrame], Color.White);
                            break;
                        case Anim.Melee:
                            spriteBatch.Draw(spriteSheet, original = new Rectangle(original.X, original.Y, melee[currentFrame].Width, HEIGHT), melee[currentFrame], Color.White);
                            break;
                    }
                    break;
                case Direction.Left:
                    switch (animation)
                    {
                        case Anim.Idle:
                            spriteBatch.Draw(spriteSheet, original, idle[currentFrame], Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
                            break;
                        case Anim.Jumping:
                            spriteBatch.Draw(spriteSheet, original = new Rectangle(original.X, original.Y, original.Width, jumping[currentFrame].Height), jumping[currentFrame], Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
                            break;
                        case Anim.WalkLeft:
                            spriteBatch.Draw(spriteSheet, original, walking[currentFrame], Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
                            break;
                        case Anim.WalkRight:
                            spriteBatch.Draw(spriteSheet, original, walking[currentFrame], Color.White);
                            break;
                        case Anim.Melee:
                            if (currentFrame > 1)
                                spriteBatch.Draw(spriteSheet, original = new Rectangle(original.X - 25, original.Y, melee[currentFrame].Width, HEIGHT), melee[currentFrame], Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
                            else
                                spriteBatch.Draw(spriteSheet, original = new Rectangle(original.X, original.Y, melee[currentFrame].Width, HEIGHT), melee[currentFrame], Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
                            break;
                    }
                    break;
            }
        }
        public bool OnGround(Rectangle ground)
        {
            if (original.Y >= ground.Y - original.Height)
            {
                original.Y = ground.Y - original.Height;
                return true;
            }
            return false;
        }
    }
}
