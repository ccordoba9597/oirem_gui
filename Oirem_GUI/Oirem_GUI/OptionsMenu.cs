﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem
{
    class OptionsMenu : MenuManager
    {
        private Rectangle main;
        private Rectangle sound;
        private Rectangle video;

        public Rectangle Main { get { return main; } set { main = value; } }

        public OptionsMenu(Texture2D button, Texture2D overlay, Point screenCtr, Rectangle window) : base(button, overlay, screenCtr, window)
        {

        }

        public override void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            spriteBatch.Draw(button, main = new Rectangle(CenterOnScreen(rectangle), size), Color.White);
            spriteBatch.DrawString(font, "Main Menu", CenterText(font, "Main Menu", main), Color.Black);
            spriteBatch.Draw(button, sound = new Rectangle(CenterOnScreen(rectangle) + new Point(-200, 0), size), Color.White);
            spriteBatch.DrawString(font, "Sound", CenterText(font, "Sound", sound) , Color.Black);
            spriteBatch.Draw(button, video = new Rectangle(CenterOnScreen(rectangle) + new Point(200, 0), size), Color.White);
            spriteBatch.DrawString(font, "Window", CenterText(font, "Window", video), Color.Black);
        }
    }
}
