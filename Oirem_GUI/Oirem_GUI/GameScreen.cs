﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem
{
    class GameScreen : MenuManager
    {
        Rectangle pause;

        public Rectangle Pause { get { return pause; } set { pause = value; } }

        public GameScreen(Texture2D button, Texture2D overlay, Point screenCtr, Rectangle window) : base(button, overlay, screenCtr, window)
        {
            location = new Point(350, 50);
            rectangle = new Rectangle(location, size);
        }

        public override void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            spriteBatch.Draw(button, pause = new Rectangle(CenterOnScreen(rectangle), rectangle.Size), Color.White);
            spriteBatch.DrawString(font, "Pause", CenterText(font, "Pause", pause), Color.Black);
            spriteBatch.DrawString(font, "Health:", new Vector2(0), Color.Black);
        }

    }
}
