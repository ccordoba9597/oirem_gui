﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem_GUI
{
    class Buttons
    {
        private Rectangle play;
        private Rectangle options;
        private Rectangle quit;
        private Rectangle main;
        private Rectangle resume;
        private Point size;
        private Point screenCtr;

        public Rectangle Play { get { return play; } set { play = value; } }
        public Rectangle Resume { get { return resume; } set { resume = value; } }
        public Rectangle Options { get { return options; } set { options = value; } }
        public Rectangle Quit { get { return quit; } set { quit = value; } }
        public Rectangle Main { get { return main; } set { main = value; } }
        public Point Size { get { return size; } set { size = value; } }

        public Buttons(Point center)
        {
            screenCtr = center;
            size = new Point(150, 50);
        }

        // Centers rectangle in window
        public void CenterOnScreen(SpriteBatch spriteBatch, Rectangle rectangle, Texture2D texture)
        {
            Point dist = rectangle.Location - rectangle.Center;
            Point loc = screenCtr + dist;
            spriteBatch.Draw(texture, rectangle = new Rectangle(loc, size), Color.White);

        }
        // Moves rectangle a specified distance from center of window
        public void CenterOnScreen(SpriteBatch spriteBatch, Rectangle rectangle, Point displacement, Texture2D texture)
        {
            Point dist = rectangle.Location - rectangle.Center;
            Point loc = (screenCtr + dist) + displacement;
            spriteBatch.Draw(texture, rectangle = new Rectangle(loc, size), Color.White);
        }

        // Centers text in object
        public void CenterText(SpriteFont font, String text, Rectangle rectangle, SpriteBatch spriteBatch, Color color)
        {
            Vector2 txtCntr = font.MeasureString(text) / 2;
            Vector2 center = new Vector2(rectangle.X + rectangle.Width / 2, rectangle.Y + rectangle.Height / 2) - txtCntr;
            spriteBatch.DrawString(font, text, center, color);
        }
        // Moves text a specified distance from center of object
        public void CenterText(SpriteFont font, String text, Rectangle rectangle, Vector2 displacement, SpriteBatch spriteBatch, Color color)
        {
            Vector2 txtCntr = font.MeasureString(text) / 2;
            Vector2 center = new Vector2(rectangle.X + rectangle.Width / 2, rectangle.Y + rectangle.Height / 2) - txtCntr;
            spriteBatch.DrawString(font, text, center + displacement, color);
        }
    }
}
