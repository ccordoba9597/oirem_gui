﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem
{
    public class MenuManager
    {
        protected Texture2D button;
        protected Texture2D overlay;
        protected Point size;
        protected Point location;
        protected Rectangle rectangle;
        protected Rectangle window;
        protected Point screenCtr;

        public Texture2D Button { get { return button; } }
        public Point Size { get { return size; } }
        public Point Location { get { return location; } }
        public Rectangle Rectangle { get { return rectangle; } }
        public int X { get { return rectangle.X; } set { rectangle.X = value; } }
        public int Y { get { return rectangle.Y; } set { rectangle.Y = value; } }

        public MenuManager(Texture2D button, Texture2D overlay, Point screenCenter, Rectangle window)
        {
            this.button = button;
            this.overlay = overlay;
            screenCtr = screenCenter;
            size = new Point(150, 50);
            location = new Point(350, 50);
            rectangle = new Rectangle(location, size);
            this.window = window;
        }

        public virtual void Update(MouseState mouse)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            
        }

        // Centers rectangle in window
        public Point CenterOnScreen(Rectangle rectangle)
        {
            Point dist = rectangle.Location - rectangle.Center;
            return screenCtr + dist;
        }

        // Centers text in object
        public Vector2 CenterText(SpriteFont font, String text, Rectangle rectangle)
        {
            Vector2 txtCntr = font.MeasureString(text) / 2;
            Vector2 center = new Vector2(rectangle.X + rectangle.Width / 2, rectangle.Y + rectangle.Height / 2) - txtCntr;
            return center;
        }
    }
}
