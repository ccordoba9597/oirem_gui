﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem
{
    class PauseMenu : MenuManager
    {
        private Rectangle resume;
        private Rectangle options;
        private Rectangle quit;
        private Rectangle main;
        private Rectangle underlay;

        public Rectangle Resume { get { return resume; } set { resume = value; } }
        public Rectangle Options { get { return options; } }
        public Rectangle Quit { get { return quit; } }
        public Rectangle Main { get { return main; } }

        public PauseMenu(Texture2D button, Texture2D overlay, Point screenCtr, Rectangle window) : base(button, overlay, screenCtr, window)
        {
            Point point = new Point(rectangle.Width + 15, rectangle.Height * 5);
            underlay = new Rectangle(rectangle.Location, point);
        }

        public override void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            spriteBatch.Draw(overlay, underlay = new Rectangle(CenterOnScreen(underlay) + new Point(-1, 25), underlay.Size), Color.White);
            spriteBatch.Draw(button, resume = new Rectangle(CenterOnScreen(rectangle) + new Point(0, -50), size), Color.White);
            spriteBatch.DrawString(font, "Resume", CenterText(font, "Resume", resume), Color.Black);
            spriteBatch.Draw(button, options = new Rectangle(CenterOnScreen(rectangle) + new Point(0, 0), size), Color.White);
            spriteBatch.DrawString(font, "Options", CenterText(font, "Options", options), Color.Black);
            spriteBatch.Draw(button, main = new Rectangle(CenterOnScreen(rectangle) + new Point(0, 50), size), Color.White);
            spriteBatch.DrawString(font, "Main Menu", CenterText(font, "Main Menu", main), Color.Black);
            spriteBatch.Draw(button, quit = new Rectangle(CenterOnScreen(rectangle) + new Point(0, 100), size), Color.White);
            spriteBatch.DrawString(font, "Quit", CenterText(font, "Quit", quit), Color.Black);
        }
    }
}
